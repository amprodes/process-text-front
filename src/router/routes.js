import DashboardLayout from "@/layout/dashboard/DashboardLayout.vue";
// GeneralViews
import NotFound from "@/pages/NotFoundPage.vue";
import SampleLayout from "@/layout/starter/SampleLayout.vue";

// Admin pages
const Login = () => import(/* webpackChunkName: "login" */"@/pages/Login.vue");
const Register = () => import(/* webpackChunkName: "register" */"@/pages/Register.vue");
const Dashboard = () => import(/* webpackChunkName: "dashboard" */"@/pages/Dashboard.vue"); 
const Notifications = () => import(/* webpackChunkName: "common" */"@/pages/Notifications.vue");
const ProcessText = () => import(/* webpackChunkName: "common" */ "@/pages/ProcessText.vue");
const ViewText = () => import(/* webpackChunkName: "common" */ "@/pages/ViewText.vue"); 
const TableList = () => import(/* webpackChunkName: "common" */ "@/pages/TableList.vue");

const routes = [
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/dashboard",
    children: [
      {
        path: "dashboard",
        name: "dashboard",
        component: Dashboard
      }, 
      {
        path: "notifications",
        name: "notifications",
        component: Notifications
      },
      {
        path: "processtext",
        name: "Procesar Texto",
        component: ProcessText
      },  
      {
        path: "table-list",
        name: "table-list",
        component: TableList
      },
      {
        path: "viewtext",
        name: "view text",
        component: ViewText
      }
    ]
  }, 
  { path: "*", component: NotFound },
  { path: "/", component: DashboardLayout, redirect: '/dashboard', children: [{ path: "/dashboard",  component: Dashboard}]},
  { path: "/login", component: SampleLayout, children: [{ path: "/login",  component: Login}]},
  { path: "/dashboard", component: DashboardLayout, children: [{ path: "/dashboard",  component: Dashboard}]},
  { path: "/register", component: SampleLayout, children: [{ path: "/register",  component: Register}]}
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
