exports.clean = string => {
    //Cleaning the string
    const alphabet = string.replace(/[^A-Za-z']+/g, " ").trim()
    const lowerCase = alphabet.toLowerCase()
    return lowerCase
}

exports.count = string => {
    let map = {}
    const words = string.split(" ").filter(word => word !== "")
    const wordsLength = string.replace(/\s/g,'').length

    //Get repeated words from string
    for (let i = 0; i < words.length; i++) {
      const item = words[i]
      map[item] = (map[item] + 1) || 1 
    }

    //Analize other data from string
    let uniqueKeys = Object.keys(Object.assign({}, ...[map]));
    let otherData = { unique:uniqueKeys.length, wordslength:wordsLength }
    
    //Joining all the data
    return { ...{map}, ...{otherData} }
}
