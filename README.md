# process-text-front

Vue.js + Nodejs for process-text-front

### Built With

This section show frameworks/libraries used to bootstrap the project.

* [Vue.js](https://vuejs.org/)
* [Express.js](https://expressjs.com/)
* [sequalize.js](https://sequelize.org/)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->
## Getting Started

This is how you set up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

This is a list of software you need to use the app.
* npm
* nodejs
* vuejs

### Installation

_Below is an example of how you can install and setting up your app._
 
1. Clone the repo
   ```sh
   git clone git@gitlab.com:amprodes/process-text-front.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ``` 
4. Now you can run the project with the following command in dev enviroment
   ```sh
   npm run dev
   ```
<p align="right">(<a href="#top">back to top</a>)</p>